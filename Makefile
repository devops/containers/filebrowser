SHELL = /bin/bash

build_tag ?= filebrowser

.PHONY: build
build:
	docker build -t $(build_tag) ./src

.PHONY: clean
clean:
	rm -rf ./log/*

.PHONY: test
test:
	build_tag=$(build_tag) ./test.sh
