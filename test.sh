#!/bin/bash

container="${CI_COMMIT_SHORT_SHA:-filebrowser}-test"

docker run --rm --name $container -p 8080:8080 -d $build_tag

code=1
interval=5
timeout=60

SECONDS=0
while [[ $SECONDS -lt $timeout ]]; do
    echo "Checking health status ... "

    if curl -sf -o /dev/null http://localhost:8080/health; then
	code=0
	break
    fi

    sleep 1
done

echo "Cleaning up ..."
docker stop $container

if [[ $code -eq 0 ]]; then
    echo "SUCCESS"
else
    echo "FAILURE"
fi

exit $code
