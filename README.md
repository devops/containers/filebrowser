# filebrowser custom image

See https://filebrowser.org for more information on File Browser.

## How to Use

Note that `FB_*` env vars are used by filebrowser as configuration
settings.

- The file hierarchy to manage should be mounted at `/data` in the filebrowser container
(i.e., `FB_SCOPE`).
- The filebrowser database, audit logs and configuration setting live in `/var/local/filebrowser`.
  You probably want to use bind mount or Docker volume to persist this information.
- You can effectively rotate the audit log by restarting the container, since the entrypoint
  sets the log file name to include the current date.
